import os
import sys

sys.path.insert(0, os.path.abspath('..'))

from app.lists.bc2 import BypassCensorship2
from app.lists.bridgelines import Bridgelines
from app.lists.mirror_mapping import MirrorMapping

with open("../schemas/bc2.json", "w") as out:
    out.write(BypassCensorship2.schema_json(indent=2))

with open("../schemas/bridgelines.json", "w") as out:
    out.write(Bridgelines.schema_json(indent=2))

with open("../schemas/mirror-mapping.json", "w") as out:
    out.write(MirrorMapping.schema_json(indent=2))
