Web Proxies
===========

Web proxies provide alternate URLs to access censored resources.
These can be accessed through a normal web browser.
They use frequently changing URLs to evade censorship.
Some functionality on more complex websites may not work fully.
The URLs will have a limited lifetime as, once discovered, they can be blocked by censors.

This assumption of a limited lifetime is built-in to the system, allowing for automated block detection to trigger the
deployment of new URLs, and for the :doc:`distribution lists <lists>` to allow applications and end-users to discover
new URLs.

Web Origins
-----------

Static Origins
--------------

These have not yet been implemented.

Simple Proxies
--------------

Smart Proxies
-------------

Where a simple proxy leads to a broken user experience, it may be necessary to use a smart proxy to mitigate the
brokenness.
