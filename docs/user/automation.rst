Automation
==========

When changes are made in the portal, the effects usually do not take place immediately, but rather
are stored in the database for an automation job to action shortly after.
There are many automation jobs that run in the portal, and you can find an overview of them
on the "Automation" page, under "Monitoring" in the sidebar:

.. image:: /_static/automation/list.png
   :width: 800

This page lists all the automation jobs, their current status, if they are enabled, their last,
and their next run times.
The last run time is updated after the completion of a run, and the next run time indicates the earliest
time that a job may be started by the scheduler, although it may not start for up to a minute after this time.

Once a task has started, the status will change from idle (🕰️) to running (🏃). If the run completes successfully,
it will revert to idle again until the next run.
If an error occurred (💥) then the module will automatically be disabled to allow for an administrator to investigate.