Tor Bridges
===========

A Tor bridge is a special type of node on the Tor network that is not listed in the public directory of nodes.
This makes it harder for governments and other organizations to block access to the Tor network, because users can
still connect to the network through these unlisted bridges. Bridges are often used in countries where access to the
Tor network is heavily restricted, as they can provide a way for users to access the network and the anonymity and
privacy it offers.

As with other circumvention resources, the dashboard takes the approach of managing collections of resources that are
uniformly configured and expendable. For this reason the only configuration that can be performed is at a high level
with the management of individual resources taking place through the automation subsystem.

Once your administrator has provided you access to the portal, you can begin to configure your Tor Bridge deployments.
To get started, select "Tor Bridges" under "Configuration" from the menu on the left hand side. If you are using a
mobile device, you may need to click the hamburger icon at the top of the screen to open the menu.

.. image:: /_static/bridges/list.png
   :width: 800

New Configuration
-----------------

To create a new configuration, click "Create new configuration" at the top of the configuration list. This will present
you with the new configuration form:

.. image:: /_static/bridges/new.png
   :width: 800

Provider
""""""""

The provider that the bridges should be deployed to. Your administrator must have configured this provider before any
resources will be deployed.

Distribution Method
"""""""""""""""""""

The distribution method for `BridgeDB <https://bridges.torproject.org/>`_.
Unless you have specific requirements you will likely choose either "Any" to allow for BridgeDB to allocate the bridge
to where it is most needed, or "None" to have a bridge that is not distributed by BridgeDB for you to distribute
directly to the end-user via another channel.

Description
"""""""""""

A free-form text description to help identify the collection.

Group
"""""

The group the collection belongs to.

Number
""""""

The number of bridges to deploy. When editing, increasing or decreasing this number will cause new bridges to be
created, or existing bridges to be destroyed, so that the number deployed will match this number.

Edit Configuration
------------------

.. image:: /_static/bridges/edit.png
   :width: 800

See the "New Configuration" section above for the descriptions of each field.

Destroy Configuration
---------------------

.. image:: /_static/bridges/destroy.png
   :width: 800

When destroying a configuration, the bridges deployed for that configuration will be automatically destroyed with it.

Bridge Rotation
---------------

.. image:: /_static/bridges/block.png
   :width: 800

If not using an automated block detection and replacement system, bridges can be manually replaced as needed.
When viewing the bridge list, either while editing the configuration or the full list of bridges, select "Mark as
blocked".
The bridge will be replaced on the next automation run.
