from app.extensions import db


class TerraformState(db.Model):  # type: ignore
    key = db.Column(db.String, primary_key=True)
    state = db.Column(db.String)
    lock = db.Column(db.String)
