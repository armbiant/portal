"""
Bypass Censorship Resource Names.
"""

from __future__ import annotations
from dataclasses import dataclass, field
from typing import Any

from flask import current_app

from app.brm.utils import is_integer


def global_namespace() -> str:
    return str(current_app.config["GLOBAL_NAMESPACE"])


@dataclass
class BRN:
    group_id: int
    product: str
    provider: str
    resource_type: str
    resource_id: str
    global_namespace: str = field(default_factory=global_namespace)

    @classmethod
    def from_str(cls, string: str) -> BRN:
        parts = string.split(":")
        if len(parts) != 6 or parts[0].lower() != "brn" or not is_integer(parts[2]):
            raise TypeError(f"Expected a valid BRN but got {repr(string)} (invalid parts).")
        resource_parts = parts[5].split("/")
        if len(resource_parts) != 2:
            raise TypeError(f"Expected a valid BRN but got {repr(string)} (invalid resource parts).")
        return cls(
            global_namespace=parts[1],
            group_id=int(parts[2]),
            product=parts[3],
            provider=parts[4],
            resource_type=resource_parts[0],
            resource_id=resource_parts[1]
        )

    def __eq__(self, other: Any) -> bool:
        return str(self) == str(other)

    def __str__(self) -> str:
        return ":".join([
            "brn",
            self.global_namespace,
            str(self.group_id),
            self.product,
            self.provider,
            f"{self.resource_type}/{self.resource_id}"
        ])

    def __repr__(self) -> str:
        return f"<BRN {str(self)}>"
